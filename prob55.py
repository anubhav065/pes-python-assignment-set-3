try:
    pound = float(input("Enter weight in pounds: "))
    errors = []
    if pound < 0:
        errors.append("Negative weight value!")
    if pound > 100:
        errors.append("Weight input more than 100")
    assert not errors, "errors are {}".format(errors)
except ValueError:
    print("Please enter value in float")
else:
    kg = pound*(1/2.2046226218)
    print("Converted Value: {}".format(kg))