from calendar import TextCalendar
import calendar
t = TextCalendar()
print(t.formatyear(2016, c=10))
print("No of leap days between 1980 and 2025: {}".format(calendar.leapdays(1980, 2025)))

while True:
    try:
        year = int(input("Enter the year to check for leap condition: "))
    except ValueError:
        print("Please enter a valid year!")
    else:
        if(calendar.isleap(year)):
            print("Yes")
            break
        else:
            print("No")
            break
        
while True:
    try:
        month = int(input("Enter the month whose 2016 calendar you want to see: "))
    except ValueError:
        print("Please enter a value between 1-12")
    else:
        print(t.formatmonth(2016, month))
        break
    