answers = lambda num1,num2: (num1 + num2, num1 - num2, num1*num2, num1/num2)
 
try:
    num1 = int(input("Enter first number"))
    num2 = int(input("Enter second number"))
    num1/num2
 
except ValueError:
    print("PLease enter an integer")
 
except ZeroDivisionError:
    print("denominator cannot be zero!")
 
else:
    add, sub, pro, quo = answers(num1,num2)
    print("sum is {}".format(add))
    print("difference is {}".format(sub))
    print("product is {}".format(pro))
    print("quotient is {}".format(quo))