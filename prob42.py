def fib(n):
    first = 0
    second = 1
    if n==0:
        print("---")
    elif n==1:
        print(first)
    elif n==2:
        print(first)
        print(second)
    else:
        print(first)
        print(second)
        while n>2:
            next_ = first+second
            print(next_)
            first = second
            second = next_
            n -= 1
            
while True:
    try:
        num = int(input("Enter the number of terms for fib series: "))
    except ValueError:
        print("please enter an integer")
    else:
        fib(num)
        break
            