print("Current content of txt file...")
my_file = open("test.txt",'r')
print(my_file.read())
my_file.close()

my_file = open("test.txt",'w+')
my_file.write("Writing first line\n")
my_file.write("Writing second line\n")
my_file.write("Writing third line\n")
my_file.write("Writing fourth line\n")
my_file.write("Writing fifth line\n")
my_file.close()

print("Current content of txt file...")
my_file = open("test.txt",'r')
print(my_file.read())
my_file.close()

my_file = open("test.txt", 'a')
my_file.write("appending first line of text\n")
my_file.write("appending second line of text\n")
my_file.write("appending third line of text\n")
my_file.write("appending fourth line of text\n")
my_file.write("appending fifth line of text\n")

print("Current content of txt file...")
my_file = open("test.txt",'r')
print(my_file.read())
my_file.close()