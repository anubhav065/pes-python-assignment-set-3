print("Current content of txt file...")
my_file = open("test.txt",'r')
print(my_file.read())
# my_file.seek(0)
my_file.close()

my_file = open("test.txt",'r')
print("Let's try to write some new content in the txt file!")
x = input("Write something that will be written in the txt file: ")
try:
	my_file.write(x)
except IOError:
	print("Oops! The file is open in read mode, we cannot write new lines in it!")
except ValueError:
	print("You have not entered a valid input!")
else:
	print("Text file has been updated!")
my_file.close()

my_file = open("test.txt",'r')
print("Let's see if our file has numbers in it!")
lines = my_file.readlines()
for x in lines:
	for y in list(x):
		try:
			print('Number found: {}'.format(int(y)))
		except ValueError:
			print('{} is not a number'.format(y))

my_file.close()