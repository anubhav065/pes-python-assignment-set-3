def extend(tuple_, list_):
    new_list = list(tuple_)
    for i in list_:
        new_list.append(i)
        
    new_tuple = tuple(new_list)
    return new_tuple
    
list_ = []
tuple_ = (6,7,8,9,10)
print('Current values in tuple are {}\n'.format(tuple_))
print('Creating new list to extend this tuple\n')
while True:
	x = input('Would you like to add a number to this list? y/n')
	if x.lower() == 'y':
		try:
			i = int(input('Enter a number: '))
		except ValueError:
			print('This is not a number')
			continue
		list_.append(i)
	elif x.lower() == 'n':
		break
	else:
		print('enter a valid option please!')

print("New tuple after extending it with the list is {}".format(extend(tuple_, list_)))