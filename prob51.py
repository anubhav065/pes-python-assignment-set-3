import glob
import os

cwd = os.getcwd()+"/"
txt_files = glob.glob(cwd+"*.txt")
len_ = len(cwd)
dic = {}
for file in txt_files:
	count = 0
	my_file = open(file,'r')
	lines = my_file.readlines()
	for line in lines:
		words = line.split()
		for word in words:
			if word == 'Treasure':
				count += 1

	dic[file[len(cwd)-1:]] = count
	my_file.close()

sum_ = dic.values()
total = 0
for i in sum_:
	total += i

print("Total number of times 'Treasure' was found in all .txt files: {}".format(total))

print("Number of times 'Treasure' was found in each file")
for k, v in dic.items():
	print(k, v)