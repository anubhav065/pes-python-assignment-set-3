def biggest(first_num=0, second_num=0, third_num=0, fourth_num=0):
    if first_num == second_num == third_num == fourth_num:
        print("All are equal")
    elif first_num > second_num:
        if first_num > third_num:
            if first_num >  fourth_num:
                print(first_num)
            else:
                print(fourth_num)
        else:
            if third_num > fourth_num:
                print(third_num)
            else:
                print(fourth_num)
    else:
        if second_num > third_num:
            if second_num > fourth_num:
                print(second_num)
            else:
                print(fourth_num)
        else:
            if third_num > fourth_num:
                print(third_num)
            else:
                print(fourth_num)

try:
    num1 = int(input("Enter first number"))
    num2 = int(input("Enter second number"))
    num3 = int(input("Enter third number"))
    num4 = int(input("Enter fourth number"))
except ValueError:
    print("Please enter valid numbers!")
else:
    biggest(num1, num2, num3, num4)