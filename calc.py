import math

def add(a,b):
    return a+b
    
def sub(a,b):
    return a-b
    
def pro(a,b):
    return a*b
    
def int_div(a,b):
    try:
        return a//b
    except ZeroDivisionError:
        print("Division by zero!")

def floor_div(a,b):
    try:
        return a/b
    except ZeroDivisionError:
        print("Division by zero!")
        
def mod(a,b):
    try:
        return a%b
    except ZeroDivisionError:
        print("Division by zero!")
        
def square_root(a):
    return math.sqrt(a)
    
def isPrime(a):
    i = 2
    while i <= sqrt(a):
        if a%i == 0:
            return 0
        i += 1
    return 1
    
def fib(terms):
    list_ = []
    first = 0
    second = 1
    if terms == 0:
        return None
    elif terms == 1:
        return first
    elif terms == 2:
        list_.append(first)
        list_.append(second)
        return list_
    else:
        list_.append(first)
        list_.append(second)
        while terms > 2:
            next_ = first + second
            list_.append(next_)
            first = second
            second = next_
            terms -= 1
    return list_