from stringop import sort, search, reverse

string = input("Enter a string to reverse: ")
list_ = []


while True:
	x = input("Would you like create a list?: y/n")
	if x.lower() == 'y':
		try:
			list_.append(int(input('Enter a number')))
		except ValueError:
			print("You have not entered a number")
	elif x.lower() == 'n':
		break
	else:
		print("Please enter a valid option!")

sort(list_)
search(list_)
reverse(string)