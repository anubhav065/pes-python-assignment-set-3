def sort(list_):
    for i in range(len(list_)-1):
        for j in range(i+1, len(list_)):
            if list_[i] > list_[j]:
                temp = list_[i]
                list_[i] = list_[j]
                list_[j] = temp
            
    return list_
    
def search(list_):
    new_list = sort(list_)
    
    try:
        number = int(input("Enter the number you want to search for in the list?: "))
    except ValueError:
        print("Please enter an integer")
    else:
        while True:
            if len(list_) == 0:
                print("List is empty so you number is not in there!")
                break
            else:
                check = list_[len(list_)//2]
                if len(list_) == 1:
                    if number == list_[0]:
                        print("{} is in the list".format(number))
                        break
                    else:
                        print("{} is not in the list".format(number))
                        break
                else:
                    if number >= list_[len(list_)//2]:
                        list_ = list_[len(list_)//2:]
                    elif number < list_[len(list_)//2]:
                        list_ = list_[0:len(list_)//2]
                    
def reverse(string):
    print("reversed string is: {}".format(string[::-1]))
    
