import math

def add(first, second):
    return first+second
    
def sub(first, second):
    return first-second
    
def pro(first, second):
    return first*second
    
def quo(first, second):
    try:
        return first/second
    except ZeroDivisionError:
        print("Denominator is 0. Will result in an error")

def square_root(**kwargs):
    print("The square root of {} is {}: ".format(kwargs['number'], math.sqrt(kwargs['number'])))
        
def substring(string, delimiter = " "):
    list_ = string.split(delimiter)
    return list_
    
try:
    first_number = int(input("Enter first number: "))
    second_number = int(input("Enter second number: "))
    third_number = int(input("Enter a number whose square root you want to find: "))
    string = input("Enter a string: ")
    delimiter = input("Enter a delimiter: ")
except ValueError:
    print("Please enter an integer")
else:
    print("sum is {}".format(add(first_number, second_number)))
    print("difference is {}".format(sub(first_number, second_number)))
    print("product is {}".format(pro(first_number, second_number)))
    print("quotient is {}".format(quo(first_number, second_number)))
    square_root(number=third_number)
    print("list of substring is {}".format(substring(string, delimiter)))
    

    
