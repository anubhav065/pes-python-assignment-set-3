print("READING LINES OF TXT FILE IN REVERSE ORDER....")
my_file = open("reading.txt",'r')
lines = my_file.readlines()
for i in range(len(lines)-1, -1, -1):
	print(lines[i])

my_file.close()

print("READING CHARACTERS OF TXT FILE IN REVERSE ORDER....")
my_file = open("reading.txt",'r')
lines = my_file.read()
print(lines[::-1])

my_file.close()