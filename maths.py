from calc import add, sub, int_div, fib, square_root
#import calc

try:
    first_num = int(input("Enter first number"))
    second_num = int(input("Enter second number"))
    terms = int(input("Enter number of terms for fibonacci series"))
except ValueError:
    print("Please enter integer values")
else:
    print("sum is {}".format(add(first_num, second_num)))
    print("subracting second number from first number: {}".format(sub(first_num, second_num)))
    print("integer division quotient is {}".format(int_div(first_num, second_num)))
    print("Square root of first number is: {}".format(square_root(first_num)))
    print("{} fibonacci terms are: {}".format(terms, fib(terms)))